/*
 * Copyright 2015 - 2020 TU Dortmund
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.learnlib.alex.modelchecking.dao;

import de.learnlib.alex.auth.entities.User;
import de.learnlib.alex.common.exceptions.NotFoundException;
import de.learnlib.alex.data.entities.Project;
import de.learnlib.alex.modelchecking.entities.LtsFormula;
import de.learnlib.alex.modelchecking.entities.LtsFormulaSuite;
import de.learnlib.alex.modelchecking.repositories.LtsFormulaRepository;
import net.automatalib.modelcheckers.ltsmin.LTSminLTLParser;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional(rollbackOn = Exception.class)
public class LtsFormulaDAO {

    private final LtsFormulaRepository ltsFormulaRepository;
    private final LtsFormulaSuiteDAO ltsFormulaSuiteDAO;

    /**
     * Constructor.
     *
     * @param ltsFormulaRepository
     *         {@link #ltsFormulaRepository}
     */
    @Autowired
    public LtsFormulaDAO(LtsFormulaRepository ltsFormulaRepository,
                         LtsFormulaSuiteDAO ltsFormulaSuiteDAO) {
        this.ltsFormulaRepository = ltsFormulaRepository;
        this.ltsFormulaSuiteDAO = ltsFormulaSuiteDAO;
    }

    public LtsFormula create(User user, Long projectId, Long suiteId, LtsFormula formula) throws NotFoundException {
        LTSminLTLParser.requireValidIOFormula(formula.getFormula());

        final LtsFormulaSuite suite = ltsFormulaSuiteDAO.get(user, projectId, suiteId);

        final LtsFormula f = new LtsFormula();
        f.setFormula(formula.getFormula());
        f.setName(formula.getName());
        f.setSuite(suite);

        return ltsFormulaRepository.save(f);
    }

    public LtsFormula update(User user, Long projectId, Long suiteId, LtsFormula formula) throws NotFoundException {
        LTSminLTLParser.requireValidIOFormula(formula.getFormula());

        final LtsFormulaSuite suite = ltsFormulaSuiteDAO.get(user, projectId, suiteId);
        final LtsFormula formulaInDb = ltsFormulaRepository.findById(formula.getId()).orElse(null);
        checkAccess(user, suite.getProject(), suite, formulaInDb);

        formulaInDb.setName(formula.getName());
        formulaInDb.setFormula(formula.getFormula());

        return ltsFormulaRepository.save(formulaInDb);
    }

    public List<LtsFormula> updateParent(User user, Long projectId, Long suiteId, List<Long> formulaIds, LtsFormulaSuite targetSuite) {
        final LtsFormulaSuite oldSuite = ltsFormulaSuiteDAO.get(user, projectId, suiteId);
        final LtsFormulaSuite newSuite = ltsFormulaSuiteDAO.get(user, projectId, targetSuite.getId());

        final List<LtsFormula> formulas = ltsFormulaRepository.findAllBySuite_IdAndIdIn(suiteId, formulaIds);
        for (LtsFormula f: formulas) {
            checkAccess(user, oldSuite.getProject(), oldSuite, f);
            f.setSuite(newSuite);
        }

        return ltsFormulaRepository.saveAll(formulas);
    }

    public void delete(User user, Long projectId, Long suiteId, Long formulaId) throws NotFoundException {
        final LtsFormulaSuite suite = ltsFormulaSuiteDAO.get(user, projectId, suiteId);
        final LtsFormula formula = ltsFormulaRepository.findById(formulaId).orElse(null);
        checkAccess(user, suite.getProject(), suite, formula);

        ltsFormulaRepository.deleteById(formulaId);
    }

    public void delete(User user, Long projectId, Long suiteId, List<Long> formulaIds) throws NotFoundException {
        for (final Long id : formulaIds) {
            delete(user, projectId, suiteId, id);
        }
    }

    public void checkAccess(User user, Project project, LtsFormulaSuite suite, LtsFormula formula) throws NotFoundException {
        ltsFormulaSuiteDAO.checkAccess(user, project, suite);

        if (formula == null) {
            throw new NotFoundException("The formula could not be found.");
        }

        if (!suite.getId().equals(formula.getSuite().getId())) {
            throw new UnauthorizedException("You are not allowed to access the resource.");
        }
    }
}
